<?php

namespace Drupal\media_entity_podbean\Plugin\media\Source;

use Drupal\media\Plugin\media\Source\OEmbed;

/**
 * Podbean media entity source definition.
 *
 * @MediaSource(
 *   id = "podbean",
 *   label = @Translation("Podbean"),
 *   description = @Translation("Embed a podcast from Podbean."),
 *   providers = {"Podbean"},
 *   allowed_field_types = {"string"},
 * )
 */
class Podbean extends OEmbed {}
