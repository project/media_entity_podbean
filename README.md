CONTENTS OF THIS FILE
---------------------

* Introduction
* Requirements
* Installation
* Configuration
* Maintainers


INTRODUCTION
------------

Media Entity Podbean module creates a media source for
Podbean so you can embed podcasts in Drupal as Media.

* For a full description of the module, visit the project page:
  https://www.drupal.org/project/media_entity_podbean

* To submit bug reports and feature suggestions, or to track changes:
  https://www.drupal.org/project/issues/media_entity_podbean


REQUIREMENTS
------------

This module requires the core Media module installed.


INSTALLATION
------------

* Install the module as you would normally install a contributed Drupal
  module.


CONFIGURATION
-------------

    1. Navigate to Administration > Structure > Content Types.
    3. Create or edit a content type.
    4. Add a media reference field.
    5. Select "Podbean" as the media type.
    6. Save the field.
    7. Start populating this field with Podbean URLs.

The oEmbed formatter will take care of displaying the podcast for you
on nodes or other entities referencing media where this is configured.

Note you must use a permalink URL from Podbean in the format
of https://www.podbean.com/e/12345.

See https://help.podbean.com/support/solutions/articles/25000004956-customizing-the-link-url-for-your-podcast-episode
for more information.

MAINTAINERS
-----------

Current maintainers:
* Kevin Quillen (kevinquillen) - https://kevinquillen.com

Supporting organizations:
* Velir - https://www.drupal.org/velir
